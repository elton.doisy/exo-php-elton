<?php
class Robot
{
    public function __construct()
    {
        if (!isset($GLOBALS['names'])) {
            $GLOBALS['names'] = [];
        }
    
    }
    
    public function getName()
    {
        $name = '';
        $caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        for ($i=0; $i < 2 ; $i++) { 
           $name .= $caracteres[rand(0,25)];
        }

        for ($i=0; $i < 3 ; $i++) { 
            $name .= rand(0,9);
        }

        $result = $this->isUnique($name);
        if ($result) {
            return $name;
        }
        $this->getName();
        return;
    }

    public function reset(): void
    {
        do {
            $name = $this->getName();
            $result = $this->isUnique($name);
        } while ($result);
    }

    private function isUnique(string $name)
    {
        if(in_array($name,$GLOBALS['names'])){
            return false;
        }

        $GLOBALS['names'][] = $name;
        return true;
    }
}
?>