<?php

function maskify($data): string
{
    $dataLength  = strlen($data);
    if($dataLength <  6){
        return $data;
    }
    $max = $dataLength - 4;
    $arg = ["0","1","2","3","4","5","6","7","8","9"];
    $result = '';
    for ($i=0; $i < $dataLength ; $i++) { 
        if ($i === 0) {
            $result .= $data[$i];
            continue;
        }
        
        if (in_array($data[$i],$arg) && ($i < $max )) {
            $result .= "#";
            continue;
        }
        $result .= $data[$i];
    }

    return $result;
}

?>